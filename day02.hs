{-
suppose you have the following program:

1,9,10,3,2,3,11,0,99,30,40,50

For the purposes of illustration, here is the same program split into multiple lines:

1,9,10,3,
2,3,11,0,
99,
30,40,50

The first four integers, 1,9,10,3, are at positions 0, 1, 2, and 3. Together, they represent the first opcode (1, addition), the positions of the two inputs (9 and 10), and the position of the output (3). To handle this opcode, you first need to get the values at the input positions: position 9 contains 30, and position 10 contains 40. Add these numbers together to get 70. Then, store this value at the output position; here, the output position (3) is at position 3, so it overwrites itself. Afterward, the program looks like this:

1,9,10,70,
2,3,11,0,
99,
30,40,50

Step forward 4 positions to reach the next opcode, 2. This opcode works just like the previous, but it multiplies instead of adding. The inputs are at positions 3 and 11; these positions contain 70 and 50 respectively. Multiplying these produces 3500; this is stored at position 0:

3500,9,10,70,
2,3,11,0,
99,
30,40,50

Stepping forward 4 more positions arrives at opcode 99, halting the program.

Here are the initial and final states of a few more small programs:

    1,0,0,0,99 becomes 2,0,0,0,99 (1 + 1 = 2).
    2,3,0,3,99 becomes 2,3,0,6,99 (3 * 2 = 6).
    2,4,4,5,99,0 becomes 2,4,4,5,99,9801 (99 * 99 = 9801).
    1,1,1,4,99,5,6,0,99 becomes 30,1,1,4,2,5,6,0,99.
-}
-- Pt 2 490634 is too low 4330636 is correct

module Main
  where

import Control.Lens

type Memory = [Int]
type Location = Int
type InstructionPointer = Location

data Machine = Machine (Maybe InstructionPointer) Memory deriving Show

data BinOp = Add | Mul
  deriving Show

data Instruction =
  Halt |
  Binary BinOp Location Location Location
  deriving Show

memory :: Machine -> Memory
memory (Machine _ m) = m


loadMachine :: String -> Machine
loadMachine initData =
  Machine (Just 0) (memoryFromData initData)
  where
    memoryFromData = map read .splitBy ','
    -- splitBy :: Char -> String -> [String]
    splitBy delimiter = foldr f [[]]
      where
        f _ [] = []
        f c l@(x:xs) | c == delimiter = []:l
                     | otherwise = (c:x):xs

instructionAt :: Location -> Memory -> Instruction
instructionAt p m =
  case (valueAt p m) of
    99 -> Halt
    1  -> Binary Add (get (p+1)) (get (p+2)) (get (p+3))
    2  -> Binary Mul (get (p+1)) (get (p+2)) (get (p+3))
    _ -> error "bad garbage"
  where
    get loc = valueAt loc m

valueAt :: Location -> Memory -> Int
valueAt p m = m !! p

putValue :: Location -> Int -> Memory -> Memory
putValue p val m = m & element p .~ val

nextInstructionPointer :: InstructionPointer -> InstructionPointer
nextInstructionPointer p = p + 4

exec :: Instruction -> Memory -> Memory
exec Halt mem = mem
exec (Binary binop op1 op2 res) mem =
  putValue res newValue mem
  where
    newValue = (operatorFor binop) (valueAt op1 mem) (valueAt op2 mem)
    operatorFor Add = (+)
    operatorFor Mul = (*)


step :: Machine -> Machine
step m@(Machine Nothing _) = m
step (Machine (Just loc) mem) =
  Machine (nextLoc instr) (exec instr mem)
  where
    instr = instructionAt loc mem
    nextLoc Halt = Nothing
    nextLoc _ = Just (nextInstructionPointer loc)

isRunning :: Machine -> Bool
isRunning (Machine Nothing _) = False
isRunning _ = True

loadNounVerb :: Int -> Int -> Machine -> Machine
loadNounVerb noun verb (Machine loc m) =
  Machine loc (putValue 1 noun . putValue 2 verb $ m)

runUntilHalt :: Machine -> Machine
runUntilHalt = head . dropWhile isRunning . iterate step

run :: Machine -> Int
run = valueAt 0 . memory . runUntilHalt

search :: Machine -> Int -> [(Int, Int, Int)]
search m target =
  filter (\(_,_,t) -> t == target) [(a, b, run . loadNounVerb a b $ m) | a <- [0..99], b <- [0..99]]

main :: IO ()
main = do
  rawdat <- readFile "day02.data"
  let machine = loadMachine rawdat in do
    print $ runUntilHalt (loadNounVerb 12 2 machine)
    print . head $ search machine 19690720
