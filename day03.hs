module Main
  where

import Data.Char (isDigit)
import Data.Set  (toList, fromList, intersection)
import Data.List (sort)

test1 :: String
test1 = "R75,D30,R83,U83,L12,D49,R71,U7,L72\nU62,R66,U55,R34,D71,R55,D58,R83"

test2 :: String
test2 = "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51\nU98,R91,D20,R16,D67,R40,U7,R15,U6,R7"

main :: IO ()
main = do
  input <- readFile "day03.data"
  print (doMainA input)
  print (doMainB input)

doMainA :: String -> Int
doMainA input =
  head . sort . map manhattan $ intersections
  where
    [wire1, wire2] = getWires input
    manhattan (a, b) = abs a + abs b
    pointSet = fromList . wirePoints
    intersections =
      toList $ intersection (pointSet wire1) (pointSet wire2)

doMainB :: String -> Int
doMainB input =
  head . sort . map pathLength $ intersections
  where
    [wire1, wire2] = getWires input
    pathLength point =
      (stepsTo point . wirePoints $ wire1) + (stepsTo point . wirePoints $ wire2)
    stepsTo point wire =
      -- add one because we drop the last step, which we need to count.
      -- and another one because I DO NOT KNOW and I am tired so I do not care.
      (+2) . length . takeWhile (/= point) $ wire
    pointSet = fromList . wirePoints
    intersections =
      toList $ intersection (pointSet wire1) (pointSet wire2)

type Point = (Int, Int)

data Direction = R | L | U | D
  deriving (Read, Show)

data Segment = Segment Direction Int
type Wire = [Segment]

instance Show Segment where
  show (Segment d x)= show d ++ show x

instance Read Segment where
  readsPrec _ [] = []
  readsPrec _ (d:input) =
    [(Segment (read [d]) (read x), rest)]
    where
      (x, rest) = span isDigit input


-- input file has eg "R10,D8" and we want "[R10,D8]"
getWire :: String -> Wire
getWire s = read ("[" ++ s ++ "]")

getWires :: String -> [Wire]
getWires = map getWire . lines

pointsOnSegment :: Segment -> Point -> [Point]
pointsOnSegment (Segment dir len) =
  take len . drop 1 . iterate (move dir)
  where
    move L = \(x,y) -> (x-1,y)
    move R = \(x,y) -> (x+1,y)
    move U = \(x,y) -> (x,y+1)
    move D = \(x,y) -> (x,y-1)

extendPoints :: Segment -> [Point] -> [Point]
extendPoints line points =
  points ++ pointsOnSegment line (last points)

fs :: Wire -> [Point] -> [Point]
fs [] points = points
fs (seg:segs) points =
  extended ++ fs segs [last extended]
  where
    extended = points ++  pointsOnSegment seg (last points)

followSegments :: Wire -> [Point] -> [Point]
followSegments [] points = init points
followSegments (line:linez) points =
  nextLeg ++ followSegments linez [last nextLeg]
  where
    nextLeg = tail $ extendPoints line points

wirePoints :: Wire -> [Point]
wirePoints wire = drop 1 . followSegments wire  $ [(0,0)]
