-- Simple: 3228475
-- Harder: 4839845

module Main
  where

import System.Environment


calculateFuel :: Integer -> Integer
calculateFuel mass =
  max 0 (mass `div` 3 - 2)


calculateFuelPlus :: Integer -> Integer
calculateFuelPlus =
  sum . drop 1 . takeWhile (/= 0) . iterate calculateFuel

answer :: [Integer] -> String
answer masses =
  "Simple: " ++ (show simple) ++ "\n" ++
  "Harder: " ++ (show harder) ++ "\n"
  where
    simple = sum [ calculateFuel mass | mass <- masses]
    harder = sum [ calculateFuelPlus mass | mass <- masses]

main :: IO ()
main = do
  args <- getArgs
  text <- readFile $ (args !! 0)
  let module_masses = map read . lines $ text in
    putStr $ answer module_masses
