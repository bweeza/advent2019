
-- https://adventofcode.com/2019/day/4

module Main
  where

import Data.List (sort)

range :: [Int]
range = [145852..616942]  -- my personal problem data

main :: IO ()
main = do
  print . length . resultA $ range
  print . length . resultB $ range
  print "OK"

monotone :: String -> Bool
monotone s =
  s == sort s

hasRepeat :: String -> Bool
hasRepeat [] = False
hasRepeat [_] = False
hasRepeat (a:b:rest)
  | a == b = True
  | otherwise = hasRepeat (b:rest)

hasDouble :: String -> Bool
hasDouble [] = False
hasDouble [_] = False
hasDouble [a,b] = a == b
hasDouble (a:b:c:rest)
  | a == b && b /= c = True
  | a == b && b == c = hasDouble (dropWhile (== a) rest)
  | otherwise = hasDouble (b:c:rest)

resultA :: [Int] -> [String]
resultA =
  filter monotone . filter hasRepeat . map show

resultB :: [Int] -> [String]
resultB =
  filter monotone . filter hasDouble . map show
